/label ~"workflow::discussion"
/assign me

## Summary

<!-- enter brief summary text here, 2-3 sentences -->

### Use cases

<!-- At least one comprehensible use case -->

## Proposal

To be discussed.

<!-- Alternatively, you can add a full text or bullet-pointed proposal here for discussion -->

## Specification

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED",  "MAY", and "OPTIONAL" in this issue are to be interpreted *in the spirit of* [RFC 2119](https://datatracker.ietf.org/doc/html/rfc2119), even though we're not technically doing protocol design.

<!-- Bullet-point list of MUST, SHOULD, MAY, SHOULD NOT and MUST NOT. will be added as a result of the proposal discussion process. -->
